/* Init database structure */

/* création du schema  catalogue */
CREATE SCHEMA IF NOT EXISTS catalogue;

/* création du table  destinataire */

create table catalogue.destinataire
(
    id               serial,
    nom              varchar(50),
    adresse          varchar(250),
    code_postal      integer not null,
    ville            varchar(30),
    pays             varchar(30),
    primary key (id)
);