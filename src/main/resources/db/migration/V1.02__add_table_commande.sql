/* création du table  commande */

create table catalogue.commande
(
    id                   serial,
    date_livraison       timestamp,
    id_destinataire      integer not null,
    quantite             integer not null,
    prix                 double precision not null,
    primary key (id),
    CONSTRAINT fk_commande
        FOREIGN KEY (id_destinataire)
            REFERENCES destinataire (id)
);