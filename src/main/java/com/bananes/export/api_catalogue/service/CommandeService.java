package com.bananes.export.api_catalogue.service;

import com.bananes.export.api_catalogue.dto.commande.CommandeGetDTO;
import com.bananes.export.api_catalogue.dto.commande.CommandePostDTO;
import com.bananes.export.api_catalogue.entity.Commande;
import com.bananes.export.api_catalogue.mapper.CommandeMapper;
import com.bananes.export.api_catalogue.repository.CommandeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import static java.time.Instant.now;

@Service
@Slf4j
@RequiredArgsConstructor
public class CommandeService {

    private final CommandeRepository commandeRepository;
    private final CommandeMapper commandeMapper;

    /**
     * Crée une nouvelle commande
     *
     * @return la commande créée
     */
    @Transactional
    public CommandeGetDTO createCommande(CommandePostDTO commandePostDTO) throws Exception {
        Commande commande = commandeMapper.toCreateEntity(commandePostDTO);
        final Instant today = now();
        final Instant dateLivraisonMinimale = today.plus(7, ChronoUnit.DAYS);
        final Integer quantite = commande.getQuantite();
        final Double prixCalcule = quantite * 2.5;

        if (commande.getDateLivraison().isBefore(dateLivraisonMinimale)) {
            throw new Exception("La date de commande doit être, au minimum, une semaine à compter de la date du jour.");
        }

        if (quantite > 10000 || quantite <= 0) {
            throw new Exception("La quantité doit être supérieure à 0 et inférieure 10000");
        }

        if (quantite % 25 != 0) {
            throw new Exception("La quantité doit être multiple de 25");
        }

        commande.setPrix(prixCalcule);

        Commande commandeSaved = commandeRepository.save(commande);
        return commandeMapper.fromEntity(commandeSaved);
    }

    /**
     * Recherche une entité commande en fonction de l'identifiant
     *
     * @param id Identifiant de commande
     * @return l'entité commande
     * @throws Exception
     */
    public Commande findById(int id) throws Exception {
        Optional<Commande> commandeOptional = commandeRepository.findById(id);
        if (commandeOptional.isEmpty()) {
            throw new Exception(String.format("La commande (id = %s) n'a pas été trouvé", id));
        }
        return commandeOptional.get();
    }

    /**
     * Mettre à jour une commande
     *
     * @param id               identifiant de commande à modifier
     * @param commandeToUpdate la commande à mettre à jour
     * @return la commande mise à jour
     */
    @Transactional
    public CommandeGetDTO updateCommande(Integer id, CommandePostDTO commandeToUpdate) throws Exception {
        Commande commande = this.findById(id);

        this.commandeMapper.toUpdateEntity(commandeToUpdate, commande);
        return commandeMapper.fromEntity(this.commandeRepository.save(commande));
    }

    /**
     * Supprimer une commande
     *
     * @param id identifiant de commande à supprimer
     */
    @Transactional
    public void deleteCommande(Integer id) throws Exception {
        Commande commande = this.findById(id);
        this.commandeRepository.delete(commande);
    }

}
