package com.bananes.export.api_catalogue.service;

import com.bananes.export.api_catalogue.dto.commande.CommandeListGetDTO;
import com.bananes.export.api_catalogue.dto.destinataire.DestinataireGetDTO;
import com.bananes.export.api_catalogue.dto.destinataire.DestinatairePostDTO;
import com.bananes.export.api_catalogue.entity.Destinataire;
import com.bananes.export.api_catalogue.mapper.DestinataireMapper;
import com.bananes.export.api_catalogue.repository.DestinataireRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class DestinataireService {

    private final DestinataireRepository destinataireRepository;
    private final DestinataireMapper destinataireMapper;

    /**
     * Récupération de la liste de toutes les destinataires
     *
     * @return une liste des destinataires
     */
    @Transactional
    public List<DestinataireGetDTO> findAll() {
        return destinataireMapper.fromEntities(destinataireRepository.findAll());
    }

    /**
     * Récupération de la liste de toutes les commandes d'un destinataire
     *
     * @param id identifiant de destinataire
     * @return une liste des commandes
     */
    @Transactional
    public List<CommandeListGetDTO> getCommandesByDestinataireId(Integer id) throws Exception {
        Destinataire destinataire = findById(id);
        DestinataireGetDTO destinataireGetDTO = destinataireMapper.fromEntity(destinataire);
        return destinataireGetDTO.getCommandesList();
    }

    /**
     * Crée un nouveau destinataire
     *
     * @return le destinataire créé
     * @throws Exception
     */
    @Transactional
    public DestinataireGetDTO createDestinataire(DestinatairePostDTO destinatairePostDTO) throws Exception {
        Destinataire destinataire = destinataireMapper.toCreateEntity(destinatairePostDTO);

        DestinataireGetDTO destinataireGetDTO = destinataireMapper.fromEntity(destinataire);

        List<DestinataireGetDTO> destinataireListBdd = this.findAll();

                for (DestinataireGetDTO destinataireDTO : destinataireListBdd) {
                  boolean estIdentique =  Objects.equals(destinataireDTO.getNom(), destinataireGetDTO.getNom())
                          && Objects.equals(destinataireDTO.getAdresse(), destinataireGetDTO.getAdresse())
                          && Objects.equals(destinataireDTO.getCodePostal(), destinataireGetDTO.getCodePostal())
                          && Objects.equals(destinataireDTO.getVille(), destinataireGetDTO.getVille())
                          && Objects.equals(destinataireDTO.getPays(), destinataireGetDTO.getPays());
                    if(estIdentique) {
                        throw new Exception("Le destinataire est identique à un autre destinataire dans la base de données");
                    }
                }

        Destinataire destinataireSaved = destinataireRepository.save(destinataire);
        return destinataireMapper.fromEntity(destinataireSaved);
    }

    /**
     * Recherche une entité destinataire en fonction de l'identifiant
     *
     * @param id Identifiant de destinataire
     * @return l'entité destinataire
     * @throws Exception
     */
    public Destinataire findById(int id) throws Exception {
        Optional<Destinataire> destinataireOptional = destinataireRepository.findById(id);
        if (destinataireOptional.isEmpty()) {
            throw new Exception(String.format("Le destinataire (id = %s) n'a pas été trouvé", id));
        }
        return destinataireOptional.get();
    }

    /**
     * Mettre à jour un destinataire
     *
     * @param  id identifiant de destinataire à modifier
     * @param destinataireToUpdate le destinataire à mettre à jour
     * @return le destinataire mis à jour
     */
    @Transactional
    public DestinataireGetDTO updateDestinataire(Integer id,DestinatairePostDTO destinataireToUpdate) throws Exception {
        Destinataire destinataire = this.findById(id);

        this.destinataireMapper.toUpdateEntity(destinataireToUpdate, destinataire);
        return destinataireMapper.fromEntity(this.destinataireRepository.save(destinataire));
    }

    /**
     * Supprimer un destinataire
     *
     * @param  id identifiant de destinataire à supprimer
     */
    @Transactional
    public void deleteDestinataire(Integer id) throws Exception {
        Destinataire destinataire = this.findById(id);
        this.destinataireRepository.delete(destinataire);
    }
}
