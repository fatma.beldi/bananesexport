package com.bananes.export.api_catalogue.mapper;

import com.bananes.export.api_catalogue.dto.commande.CommandeGetDTO;
import com.bananes.export.api_catalogue.dto.commande.CommandePostDTO;
import com.bananes.export.api_catalogue.entity.Commande;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface CommandeMapper {
    CommandeGetDTO fromEntity(Commande commande);

    Commande toCreateEntity(CommandePostDTO commandePostDTO);

    void toUpdateEntity(CommandePostDTO commandePostDTO, @MappingTarget Commande commande);
}
