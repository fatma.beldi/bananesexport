package com.bananes.export.api_catalogue.mapper;

import com.bananes.export.api_catalogue.dto.destinataire.DestinataireGetDTO;
import com.bananes.export.api_catalogue.dto.destinataire.DestinatairePostDTO;
import com.bananes.export.api_catalogue.entity.Destinataire;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface DestinataireMapper {
    DestinataireGetDTO fromEntity(Destinataire destinataire);

    List<DestinataireGetDTO> fromEntities(Iterable<Destinataire> destinataire);


    Destinataire toCreateEntity(DestinatairePostDTO destinatairePostDTO);

    void toUpdateEntity(DestinatairePostDTO destinatairePostDTO, @MappingTarget Destinataire destinataire);
}
