package com.bananes.export.api_catalogue.repository;


import com.bananes.export.api_catalogue.entity.Destinataire;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DestinataireRepository extends JpaRepository<Destinataire, Integer> {
}
