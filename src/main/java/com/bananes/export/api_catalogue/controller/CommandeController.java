package com.bananes.export.api_catalogue.controller;

import com.bananes.export.api_catalogue.dto.commande.CommandeGetDTO;
import com.bananes.export.api_catalogue.dto.commande.CommandePostDTO;
import com.bananes.export.api_catalogue.service.CommandeService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("commandes")
@RequiredArgsConstructor
public class CommandeController {

    private final CommandeService commandeService;

    @PostMapping
    public ResponseEntity<CommandeGetDTO> createCommande(@Valid @RequestBody final CommandePostDTO commandePostDTO) throws Exception {
        return ResponseEntity.status(HttpStatus.CREATED).body(commandeService.createCommande(commandePostDTO));
    }

    @PutMapping({"{id}"})
    public ResponseEntity<CommandeGetDTO> updateCommande(@Valid @PathVariable Integer id,
                                                          @RequestBody final CommandePostDTO commandePostDTO) throws Exception {
        return ResponseEntity.ok(commandeService.updateCommande(id, commandePostDTO));
    }

    @DeleteMapping({"{id}"})
    public ResponseEntity<Void> deleteCommande(@PathVariable Integer id) throws Exception {
        commandeService.deleteCommande(id);
        return ResponseEntity.ok().build();
    }
}
