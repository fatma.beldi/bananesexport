package com.bananes.export.api_catalogue.controller;

import com.bananes.export.api_catalogue.dto.commande.CommandeListGetDTO;
import com.bananes.export.api_catalogue.dto.destinataire.DestinataireGetDTO;
import com.bananes.export.api_catalogue.dto.destinataire.DestinatairePostDTO;
import com.bananes.export.api_catalogue.service.DestinataireService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("destinataires")
@RequiredArgsConstructor
public class DestinataireController {

    private final DestinataireService destinataireService;

    @GetMapping
    public ResponseEntity<List<DestinataireGetDTO>> getAllDestinataires() {
        return ResponseEntity.ok(destinataireService.findAll());
    }

    @GetMapping("/{id}/commandes")
    public ResponseEntity<List<CommandeListGetDTO>> getCommandesByDestanataireId(@PathVariable Integer id) throws Exception {
        return ResponseEntity.ok(destinataireService.getCommandesByDestinataireId(id));
    }

    @PostMapping
    public ResponseEntity<DestinataireGetDTO> createDestinataire(@Valid @RequestBody final DestinatairePostDTO destinatairePostDTO) throws Exception {
        return ResponseEntity.status(HttpStatus.CREATED).body(destinataireService.createDestinataire(destinatairePostDTO));
    }

    @PutMapping({"{id}"})
    public ResponseEntity<DestinataireGetDTO> updateDestinataire(@Valid @PathVariable Integer id,
                                                                 @RequestBody final DestinatairePostDTO destinatairePostDTO) throws Exception {
        return ResponseEntity.ok(destinataireService.updateDestinataire(id, destinatairePostDTO));
    }

    @DeleteMapping({"{id}"})
    public ResponseEntity<Void> deleteDestinataire(@PathVariable Integer id) throws Exception {
        destinataireService.deleteDestinataire(id);
        return ResponseEntity.ok().build();
    }
}
