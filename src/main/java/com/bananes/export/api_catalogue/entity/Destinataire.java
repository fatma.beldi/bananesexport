package com.bananes.export.api_catalogue.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "destinataire")
@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Destinataire {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String nom;

    private String adresse;

    private Integer codePostal;

    private String ville;

    private String pays;

    @OneToMany( mappedBy = "destinataire", cascade = CascadeType.ALL)
    private List<Commande> commandesList;

}
