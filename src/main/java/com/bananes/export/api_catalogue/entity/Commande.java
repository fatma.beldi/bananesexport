package com.bananes.export.api_catalogue.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.Instant;

@Entity
@Table(name = "commande")
@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Commande {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Instant dateLivraison;

    private Integer quantite;

    private Double prix;

    @ManyToOne
    @JoinColumn(name = "id_destinataire", referencedColumnName = "id")
    private Destinataire destinataire;

}
