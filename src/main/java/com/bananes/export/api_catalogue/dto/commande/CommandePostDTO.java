package com.bananes.export.api_catalogue.dto.commande;

import com.bananes.export.api_catalogue.dto.destinataire.DestinataireGetDTO;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.Instant;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommandePostDTO {
    @NotNull
    private Instant dateLivraison;

    @NotNull
    private Integer quantite;

    private Double prix;

    @NotNull
    private DestinataireGetDTO destinataire;
}
