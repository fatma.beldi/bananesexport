package com.bananes.export.api_catalogue.dto.commande;

import lombok.*;

import java.time.Instant;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommandeListGetDTO {
    private Integer id;

    private Instant dateLivraison;

    private Integer quantite;

    private Double prix;
}
