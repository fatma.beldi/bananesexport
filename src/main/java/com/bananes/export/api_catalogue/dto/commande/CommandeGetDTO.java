package com.bananes.export.api_catalogue.dto.commande;

import com.bananes.export.api_catalogue.dto.destinataire.DestinataireGetDTO;
import lombok.*;

import java.time.Instant;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommandeGetDTO {
    private Integer id;

    private Instant dateLivraison;

    private Integer quantite;

    private Double prix;

    private DestinataireGetDTO destinataire;
}
