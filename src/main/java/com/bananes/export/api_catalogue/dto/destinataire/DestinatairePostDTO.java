package com.bananes.export.api_catalogue.dto.destinataire;

import jakarta.validation.constraints.NotBlank;
import lombok.*;
import jakarta.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DestinatairePostDTO {

    @NotBlank
    private String nom;

    @NotBlank
    private String adresse;

    @NotNull
    private Integer codePostal;

    @NotBlank
    private String ville;

    @NotBlank
    private String pays;
}
