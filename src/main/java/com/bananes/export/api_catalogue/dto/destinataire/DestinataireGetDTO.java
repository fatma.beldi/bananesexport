package com.bananes.export.api_catalogue.dto.destinataire;

import com.bananes.export.api_catalogue.dto.commande.CommandeListGetDTO;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DestinataireGetDTO {
    private Integer id;

    private String nom;

    private String adresse;

    private Integer codePostal;

    private String ville;

    private String pays;

    private List<CommandeListGetDTO> commandesList;
}
